# Copyright 2009 David Leverton
# Distributed under the terms of the GNU General Public License v2

# GHC >=6.10.3 *strongly recommended* by upstream
require bash-completion hackage [ has_bin=true ghc_dep='>=6.10.3' ]

SUMMARY="Distributed, interactive, smart revision control system"
DESCRIPTION="Darcs is a free, open source revision control system. It is:

* Distributed: Every user has access to the full command set, removing
  boundaries between server and client or committer and non-committers.

* Interactive: Darcs is easy to learn and efficient to use because it
  asks you questions in response to simple commands, giving you choices
  in your work flow. You can choose to record one change in a file,
  while ignoring another. As you update from upstream, you can review
  each patch name, even the full \"diff\" for interesting patches.

* Smart: Originally developed by physicist David Roundy, darcs is based
  on a unique algebra of patches.

This smartness lets you respond to changing demands in ways that would
otherwise not be possible."
DOWNLOADS="http://darcs.net/releases/${PNV}.tar.gz"

LICENCES="GPL-2"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        net-misc/curl[>=7.19.1] [[ note = [ this version required for pipelining ] ]]

    $(haskell_lib_dependencies "
        dev-haskell/array[>=0.4&<0.6]
        dev-haskell/attoparsec[>=0.11&<0.14]
        dev-haskell/base16-bytestring[>=0.1&<0.2]
        dev-haskell/binary[>=0.5&<0.9]
        dev-haskell/bytestring[>=0.9.0&<0.11]
        dev-haskell/containers[>=0.4&<0.6]
        dev-haskell/cryptohash[>=0.4&<0.12]
        dev-haskell/data-ordlist[>=0.4&<0.5]
        dev-haskell/directory[>=1.1.0.2&<1.3.0.0]
        dev-haskell/filepath[>=1.2.0.0&<1.5.0.0]
        dev-haskell/hashable[>=1.0&<1.3]
        dev-haskell/haskeline[>=0.6.3&<0.8]
        dev-haskell/html[=1.0*]
        dev-haskell/HTTP[>=4000.2.3&<4000.4]
        dev-haskell/mmap[>=0.5&<0.6]
        dev-haskell/mtl[>=2.1&<2.3]
        dev-haskell/network[>=2.3&<2.7]
        dev-haskell/network-uri[>=2.6&<2.7]
        dev-haskell/old-locale[>=1.0&<1.1]
        dev-haskell/old-time[>=1.1&<1.2]
        dev-haskell/parsec[>=3.1&<3.2]
        dev-haskell/process[>=1.1.0.1&<1.5.0.0]
        dev-haskell/random[>=1.0&<1.2]
        dev-haskell/regex-applicative[>=0.2&<0.4]
        dev-haskell/regex-compat-tdfa[>=0.95.1&<0.96]
        dev-haskell/sandi[>=0.2&<0.4]
        dev-haskell/tar[>=0.4&<0.6]
        dev-haskell/text[>=0.11.3.1&<1.3]
        dev-haskell/time[>=1.4&<1.7]
        dev-haskell/transformers[>0.4.0.0&<0.6]
        dev-haskell/transformers-compat[>=0.4&<0.6]
        dev-haskell/unix[>=2.5&<2.8]
        dev-haskell/unix-compat[>=0.1.2&<0.5]
        dev-haskell/utf8-string[>=0.3.6&<1.1]
        dev-haskell/vector[>=0.7&<0.12]
        dev-haskell/zip-archive[>=0.2.3&<0.4]
        dev-haskell/zlib[>=0.5.3.0&<0.7.0.0]
    ")

    $(haskell_test_dependencies "
        dev-haskell/HUnit[>=1.0]
        dev-haskell/QuickCheck[>=2.3]
        dev-haskell/cmdargs[>=0.10&<0.11]
        dev-haskell/FindBin[>=0.0&<0.1]
        dev-haskell/split
        dev-haskell/test-framework[>=0.4.0]
        dev-haskell/test-framework-hunit[>=0.2.2]
        dev-haskell/test-framework-quickcheck2[>=0.3]
    ")
"

CABAL_SRC_CONFIGURE_PARAMS=(
    --flags=curl
    --flags=-hashed-storage-diff
    --flags=http
    --flags=mmap
    --flags=-terminfo
    --flags=color
    --flags=test
    --flag=-use-local-data-map-strict
)

src_prepare() {
    default

    # issue2188
    edo mv "${WORK}"/tests/issue1344_abort_early_cant_send.sh "${WORK}"/tests/failing-issue1344_abort_early_cant_send.sh
}

src_install() {
    cabal_src_install
    alternatives_for ${PN} ${PV} ${PV} \
        /usr/$(exhost --target)/bin/darcs darcs-${PV} \
        /usr/share/man/man1/darcs.1 darcs-${PV}.1
    dobashcompletion contrib/darcs_completion
    edo chmod +r "${IMAGE}"/usr/share/man/man1/${PNV}.1
}

# Requires shelly, unpackaged
RESTRICT="test"

