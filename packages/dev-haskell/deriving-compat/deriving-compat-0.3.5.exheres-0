# Copyright 2017 Markus Rothe <mrothe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require hackage

SUMMARY="Backports of GHC deriving extensions"
DESCRIPTION="
Provides Template Haskell functions that mimic deriving
extensions that were introduced or modified in recent versions
of GHC. Currently, the following typeclasses/extensions are covered:

* Deriving Bounded

* Deriving Enum

* Deriving Ix

* Deriving Eq, Eq1, and Eq2

* Deriving Ord, Ord1, and Ord2

* Deriving Read, Read1, and Read2

* Deriving Show, Show1, and Show2

* DeriveFoldable

* DeriveFunctor

* DeriveTraversable

See the Data.Deriving module for a full list of backported changes.

Note that some recent GHC typeclasses/extensions are not covered by this package:

* DeriveDataTypeable

* DeriveGeneric, which was introducted in GHC 7.2 for deriving
Generic instances, and modified in GHC 7.6 to allow derivation
of Generic1 instances. Use Generics.Deriving.TH from
generic-deriving
to derive Generic(1) using Template Haskell.

* DeriveLift, which was introduced in GHC 8.0 for deriving
Lift instances. Use Language.Haskell.TH.Lift from
th-lift
to derive Lift using Template Haskell.

* The Bifunctor typeclass, which was introduced in GHC 7.10,
as well as the Bifoldable and Bitraversable typeclasses, which
were introduced in GHC 8.2. Use Data.Bifunctor.TH from
bifunctors
to derive these typeclasses using Template Haskell.
"
HOMEPAGE="https://github.com/haskell-compat/deriving-compat"

LICENCES="BSD-3"
PLATFORMS="~amd64"

DEPENDENCIES="
    $(haskell_lib_dependencies "
        dev-haskell/containers[>=0.1&<0.6]
        dev-haskell/ghc-boot-th
        dev-haskell/template-haskell[=2.11*]
        dev-haskell/transformers[=0.2*|=0.3*|>=0.5]
        dev-haskell/transformers-compat[>=0.5]
    ")
    $(haskell_test_dependencies "
        dev-haskell/QuickCheck[=2*]
        dev-haskell/base-compat[>=0.8.1&<1]
        dev-haskell/base-orphans[~>0.5]
        dev-haskell/hspec[>=1.8]
        dev-haskell/tagged[~>0.7]
        dev-haskell/template-haskell[>=2.5&<2.12]
        dev-haskell/transformers[=0.2*|=0.3*|=0.5*]
        dev-haskell/transformers-compat[>=0.5]
    ")
"

