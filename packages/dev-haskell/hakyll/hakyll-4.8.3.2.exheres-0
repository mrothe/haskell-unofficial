# Copyright 2010, 2011 Markus Rothe
# Distributed under the terms of the GNU General Public License v2

require hackage [ has_bin=true ]

SUMMARY="Simple static site generator library"

LICENCES="BSD-3"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    $(haskell_lib_dependencies "
        app-text/pandoc[>=1.14&<1.18]
        dev-haskell/binary[>=0.5&<0.9]
        dev-haskell/blaze-html[>=0.5&<0.9]
        dev-haskell/blaze-markup[>=0.5.1&<0.8]
        dev-haskell/bytestring[>=0.9&<0.11]
        dev-haskell/cmdargs[>=0.10&<0.11]
        dev-haskell/containers[>=0.3&<0.6]
        dev-haskell/cryptohash[>=0.7&<0.12]
        dev-haskell/deepseq[>=1.3.0&<1.5]
        dev-haskell/directory[>=1.0&<1.3]
        dev-haskell/filepath[>=1.0&<1.5]
        dev-haskell/fsnotify[>=0.2&<0.3]
        dev-haskell/http-conduit[>=2.1&<2.2]
        dev-haskell/http-types[>=0.7&<0.10]
        dev-haskell/lrucache[>=1.1.1&<1.3]
        dev-haskell/mtl[>=1&<2.3]
        dev-haskell/network[>=2.6&<2.7]
        dev-haskell/network-uri[>=2.6&<2.7]
        dev-haskell/pandoc-citeproc[>=0.4&<0.11]
        dev-haskell/parsec[>=3.0&<3.2]
        dev-haskell/process[>=1.0&<1.5]
        dev-haskell/random[>=1.0&<1.2]
        dev-haskell/regex-base[>=0.93&<0.94]
        dev-haskell/regex-tdfa[>=1.1&<1.3]
        dev-haskell/resourcet[>=1.1&<1.2]
        dev-haskell/scientific[>=0.3.4&<0.4]
        dev-haskell/snap-core[>=0.6&<0.10]
        dev-haskell/snap-server[>=0.6&<0.10]
        dev-haskell/system-filepath[>=0.4.6&<0.5]
        dev-haskell/tagsoup[>=0.13.1&<0.15]
        dev-haskell/text[>=0.11&<1.3]
        dev-haskell/time[>=1.4&<1.7]
        dev-haskell/time-locale-compat[>=0.1&<0.2]
        dev-haskell/unordered-containers[>=0.2&<0.3]
        dev-haskell/vector[>=0.11&<0.12]
        dev-haskell/yaml[>=0.8&<0.9]
    ")
    $(haskell_test_dependencies "
        dev-haskell/HUnit[>=1.2&<1.4]
        dev-haskell/QuickCheck[>=2.4&<2.9]
        dev-haskell/data-default[>=0.4&<0.8]
        dev-haskell/test-framework-hunit[>=0.3&<0.4]
        dev-haskell/test-framework-quickcheck2[>=0.3&<0.4]
        dev-haskell/test-framework[>=0.4&<0.9]
    ")
"

src_install() {
    cabal_src_install
    alternatives_for ${PN} ${PV} ${SLOT} /usr/$(exhost --target)/bin/hakyll-init haddock-init-${PV}
}

# Tests hang forever
RESTRICT="test"

