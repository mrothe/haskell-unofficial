# Copyright 2012 Nikolay Orlyuk
# Distributed under the terms of the GNU General Public License v2

require hackage

SUMMARY="A raw binding for the OpenGL graphics system"
DESCRIPTION="
Raw Haskell binding for the OpenGL 3.2 graphics system and lots of OpenGL extensions. It is
basically a 1:1 mapping of OpenGL's C API, intended as a basis for a nicer interface. OpenGLRaw
offers access to all necessary functions, tokens and types plus a general facility for loading
extension entries. The module hierarchy closely mirrors the naming structure of the OpenGL
extensions, making it easy to find the right module to import. All API entries are loaded
dynamically, so no special C header files are needed for building this package. If an API entry is
not found at runtime, a userError is thrown.
"
HOMEPAGE="http://www.haskell.org/haskellwiki/Opengl"

LICENCES="BSD-3"
PLATFORMS="~x86 ~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        x11-dri/mesa
    $(haskell_lib_dependencies "
        dev-haskell/bytestring[>=0.9&<0.11]
        dev-haskell/containers[>=0.3&<0.6]
        dev-haskell/fixed[>=0.2&<0.3]
        dev-haskell/half[>=0.2.2.1&<0.3]
        dev-haskell/text[>=0.1&<1.3]
        dev-haskell/transformers[>=0.2&<0.6]
    ")
"

