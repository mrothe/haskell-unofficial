# Copyright 2008 Santiago M. Mola
# Copyright 2009, 2010 Ingmar Vanhassel
# Copyright 2012 Markus Rothe
# Distributed under the terms of the GNU General Public License v2

require hackage [ has_bin=true ]

SUMMARY="A liberalised re-implementation of cpp, the C pre-processor"
DESCRIPTION="
Cpphs is a re-implementation of the C pre-processor that is both more
compatible with Haskell, and itself written in Haskell so that it can be
distributed with compilers.

This version of the C pre-processor is pretty-much feature-complete and
compatible with traditional (K&R) pre-processors. Additional features include:
a plain-text mode; an option to unlit literate code files; and an option to
turn off macro-expansion.
"
HOMEPAGE="http://www.haskell.org/cpphs/ ${HOMEPAGE}"

LICENCES="GPL-2 LGPL-2.1"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    $(haskell_lib_dependencies "
        dev-haskell/directory
        dev-haskell/old-locale
        dev-haskell/old-time
        dev-haskell/polyparse[>=1.9]
    ")
"

src_install() {
    cabal_src_install
    insinto /usr/share/doc/${PNVR}
    doins docs/index.html
    doman docs/cpphs.1
    alternatives_for ${PN} ${PV} ${SLOT} /usr/$(exhost --target)/bin/cpphs cpphs-${PV} /usr/share/man/man1/cpphs.1 cpphs-${PV}.1
}

