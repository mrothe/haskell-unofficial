# Copyright 2014 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require hackage

SUMMARY="Commonly used conduit functions, for both chunked and unchunked data"
DESCRIPTION="
Provides a replacement for Data.Conduit.List, as well as a convenient Conduit module.
"
HOMEPAGE="https://github.com/snoyberg/mono-traversable"

LICENCES="MIT"
PLATFORMS="~amd64"

DEPENDENCIES="
    $(haskell_lib_dependencies "
        dev-haskell/base16-bytestring
        dev-haskell/base64-bytestring[>=0.1.1.1]
        dev-haskell/bytestring
        dev-haskell/chunked-data[>=0.3]
        dev-haskell/conduit[>=1.2.8]
        dev-haskell/conduit-extra[>=1.1.1]
        dev-haskell/filepath
        dev-haskell/monad-control
        dev-haskell/mono-traversable[>=1.0]
        dev-haskell/mwc-random
        dev-haskell/primitive
        dev-haskell/resourcet
        dev-haskell/text
        dev-haskell/transformers
        dev-haskell/transformers-base
        dev-haskell/unix
        dev-haskell/unix-compat
        dev-haskell/vector
        dev-haskell/void
    ")
    $(haskell_test_dependencies "
        dev-haskell/QuickCheck[>=2.5]
        dev-haskell/base16-bytestring
        dev-haskell/base64-bytestring
        dev-haskell/bytestring
        dev-haskell/chunked-data
        dev-haskell/conduit
        dev-haskell/containers
        dev-haskell/directory
        dev-haskell/filepath
        dev-haskell/hspec[>=1.3]
        dev-haskell/mono-traversable
        dev-haskell/mtl
        dev-haskell/mwc-random
        dev-haskell/safe
        dev-haskell/silently
        dev-haskell/text
        dev-haskell/transformers
        dev-haskell/vector
    ")
"

# tests fail
RESTRICT="test"

