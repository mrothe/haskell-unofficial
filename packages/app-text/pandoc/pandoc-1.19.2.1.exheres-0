# Copyright 2009, 2010 Sterling X. Winter <replica@exherbo.org>
# Copyright 2011 Markus Rothe
# Distributed under the terms of the GNU General Public License v2

require hackage [ has_bin=true ]

SUMMARY="Command-line tool and Haskell library for converting from one markup format to another"
DESCRIPTION="
Pandoc is a Haskell library for converting from one markup format to another, and a command-line
tool that uses this library. It can read Markdown and (subsets of) reStructuredText, HTML, and
LaTeX, and it can write plain text, Markdown, reStructuredText, HTML, LaTeX, ConTeXt, PDF, RTF,
DocBook XML, OpenDocument XML, ODT, GNU Texinfo, MediaWiki markup, groff man pages, and S5 HTML
slide shows. PDF output (via LaTeX) is also supported with the included markdown2pdf wrapper script.
Pandoc extends standard Markdown syntax with footnotes, embedded LaTeX, definition lists, tables,
and other features. A compatibility mode is provided for those who need a drop-in replacement for
Markdown.pl.
"
HOMEPAGE="http://johnmacfarlane.net/${PN}/ ${HOMEPAGE}"

LICENCES="GPL-2"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

# Tests seem to be rather finicky: require citeproc-hs to be built with a certain flag enabled, etc.
# Some versions of Cabal also complain.
RESTRICT="test"

DEPENDENCIES="
    $(haskell_lib_dependencies "
        dev-haskell/HTTP[>=4000.0.5&<4000.4]
        dev-haskell/SHA[>=1.6&<1.7]
        dev-haskell/aeson[>=0.7&<1.2]
        dev-haskell/array[>=0.3&<0.6]
        dev-haskell/base64-bytestring[>=0.1&<1.1]
        dev-haskell/binary[>=0.5&<0.9]
        dev-haskell/blaze-html[>=0.5&<0.10]
        dev-haskell/blaze-markup[>=0.5.1&<0.9]
        dev-haskell/bytestring[>=0.9&<0.11]
        dev-haskell/cmark[>=0.5&<0.6]
        dev-haskell/containers[>=0.1&<0.6]
        dev-haskell/data-default[>=0.4&<0.8]
        dev-haskell/deepseq[>=1.3&<1.5]
        dev-haskell/directory[>=1&<1.4]
        dev-haskell/doctemplates[>=0.1&<0.2]
        dev-haskell/extensible-exceptions[>=0.1&<0.2]
        dev-haskell/filemanip[>=0.3&<0.4]
        dev-haskell/filepath[>=1.1&<1.5]
        dev-haskell/haddock-library[>=1.1&<1.5]
        dev-haskell/skylighting[>=0.1.1.4&<0.2]
        dev-haskell/hslua[>=0.3&<0.5]
        dev-haskell/http-client[>=0.3.2&<0.5]
        dev-haskell/http-client-tls[>=0.2&<0.3]
        dev-haskell/http-types[>=0.8&<0.10]
        dev-haskell/JuicyPixels[>=3.1.6.1&<3.3]
        dev-haskell/mtl[>=2.2&<2.3]
        dev-haskell/network[>=2]
        dev-haskell/network-uri[>=2.6&<2.7]
        dev-haskell/old-locale[>=1&<1.1]
        dev-haskell/old-time
        dev-haskell/pandoc-types[>=1.17&<1.18]
        dev-haskell/parsec[>=3.1&<3.2]
        dev-haskell/process[>=1&<1.5]
        dev-haskell/random[>=1&<1.2]
        dev-haskell/scientific[>=0.2&<0.4]
        dev-haskell/syb[>=0.1&<0.7]
        dev-haskell/tagsoup[>=0.13.7&<0.15]
        dev-haskell/temporary[>=1.1&<1.3]
        dev-haskell/texmath[>=0.9&<0.10]
        dev-haskell/text
        dev-haskell/time[>=1.2&<1.7]
        dev-haskell/unordered-containers[>=0.2&<0.3]
        dev-haskell/vector[>=0.10&<0.13]
        dev-haskell/wai[>=0.3]
        dev-haskell/wai-extra
        dev-haskell/xml[>=1.3.12&<1.4]
        dev-haskell/yaml[>=0.8.8.2&<0.9]
        dev-haskell/zip-archive[>=0.2.3.4&<0.4]
        dev-haskell/zlib[>=0.5&<0.7]
    ")

    $(haskell_bin_dependencies "
        dev-haskell/happy
        dev-haskell/alex
    ")

    $(haskell_test_dependencies "
        dev-haskell/Diff[>=0.2&<0.4]
        dev-haskell/HUnit[>=1.2&<1.6]
        dev-haskell/QuickCheck[>=2.4&<2.10]
        dev-haskell/ansi-terminal[>=0.5&<0.7]
        dev-haskell/test-framework[>=0.3&<0.9]
        dev-haskell/test-framework-hunit[>=0.2&<0.4]
        dev-haskell/test-framework-quickcheck2[>=0.2.9&<0.4]
    ")

    suggestion:
        app-vim/pandoc-syntax [[ description = [ Pandoc syntax highlighting for Vim ] ]]
"

src_install() {
    cabal_src_install

    expecting_tests && edo rm "${IMAGE}"/usr/$(exhost --target)/bin/test-pandoc

    local my_providers=( /usr/$(exhost --target)/bin/pandoc pandoc-${SLOT} )

    alternatives_for ${PN} ${PV} ${SLOT} "${my_providers[@]}"
}

