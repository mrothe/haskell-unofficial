# Copyright 2008 Santiago M. Mola
# Copyright 2008, 2009, 2010, 2011 Ingmar Vanhassel
# Copyright 2011 Markus Rothe
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'ghc-6.8.2.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require bash-completion flag-o-matic alternatives
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

export_exlib_phases pkg_pretend src_prepare src_configure src_install src_test_expensive pkg_prerm pkg_postinst

SUMMARY="The Glorious Glasgow Haskell Compilation System"
DESCRIPTION="
GHC is a state-of-the-art, open source, compiler and interactive environment for the functional
language Haskell.
"
HOMEPAGE="https://www.haskell.org/ghc/"
DOWNLOADS="
    https://downloads.haskell.org/~ghc/${PV}/${PNV}-src.tar.xz
    https://downloads.haskell.org/~ghc/${PV}/${PNV}-testsuite.tar.xz
"

DOWNLOADS+="
    bootstrap? (
        platform:amd64? (
            libc:glibc? ( https://slyfox.uni.cx/~slyfox/distfiles/${PN}-bin-${PV}-x86_64-pc-linux-gnu.tbz2 )
            libc:musl? ( https://www.somasis.com/dl/ghc-musl-bootstrap/${PNV}-x86_64-pc-linux-musl.tar.xz )
        )
        platform:x86? ( http://code.haskell.org/~slyfox/${PN}-x86/${PN}-bin-${PV}-x86.tbz2 )
    )
"

BUGS_TO="ingmar@exherbo.org dleverton@exherbo.org"
REMOTE_IDS="freecode:${PN}"
UPSTREAM_RELEASE_NOTES="http://www.haskell.org/${PN}/docs/${PV}/html/users_guide/release-${PV//./-}.html"

LICENCES="BSD-3"
SLOT="${PV}"
MYOPTIONS="bootstrap doc
    bootstrap [[ description = [ Bootstrap GHC from a pre-compiled binary, rather than an already installed GHC ] ]]
    llvm [[ description = [ Code generation using LLVM ] ]]
"

# `make test` takes really long, so disable it. It also succeeds (i.e. returned value is zero) in
# case of unexpected failures. There are always unexpected failures.
# If you really want to run the tests, then enable expensive tests in order to run `make fulltest`,
# which runs the same tests as `make test`, but includes more test cases per test.
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-libs/libxslt[>=1.1.2]
        dev-python/Sphinx
        bootstrap? (
            sys-libs/ncurses
        )
        doc? (
            !bootstrap? ( dev-haskell/haddock[>=0.8] )
        )
    build+run:
        dev-libs/libffi
        dev-lang/perl:*[>=5.6.1]
        dev-libs/gmp:=[>=4.1]
        bootstrap? ( dev-libs/gmp:=[>=5] )
        dev-libs/libedit
        llvm? ( dev-lang/llvm[>=2.7] )
        sys-devel/binutils:*[>=2.17]
        sys-devel/gcc:*[>=2.95.3]
"

MYOPTIONS+="
    platform: amd64 x86
    libc: glibc musl
"

append-ghc-cflags() {
    local flag compile assemble link
    for flag in ${@}; do
        case ${flag} in
            compile)  compile="yes" ;;
            assemble) assemble="yes" ;;
            link)     link="yes" ;;
            *)        [[ -n ${compile}  ]] && GHC_CFLAGS="${GHC_CFLAGS} -optc${flag}"
                      [[ -n ${assemble} ]] && GHC_CFLAGS="${GHC_CFLAGS} -opta${flag}"
                      [[ -n ${link}     ]] && GHC_CFLAGS="${GHC_CFLAGS} -optl${flag}"
                      ;;
        esac
    done
}

ghc_setup_cflags() {
    # We need to be very careful with the CFLAGS we ask ghc to pass through to gcc.
    # There are plenty of flags which will make gcc produce output that breaks ghc in various ways.
    # The main ones we want to pass through are -mcpu / -march flags.
    # These are important for arches like alpha & sparc.
    # We also use these CFLAGS for building the C parts of ghc, ie the rts.
    strip-flags
    strip-unsupported-flags
    filter-flags -fPIC

    GHC_CFLAGS=""
    for flag in ${CFLAGS}; do
        case ${flag} in
            # Arch and ABI flags are what we're really after
            -m*) append-ghc-cflags compile assemble ${flag} ;;
            # Ignore extra optimisation (ghc passes -O to gcc anyway)
            # -O2 and above break on too many systems
            -O*) : ;;
            # Debugging flags don't help either. You can't debug Haskell code
            # at the C source level and the mangler discards the debug info.
            -g*) : ;;
            # Ignore all other flags, including all -f* flags
            *)   : ;;
        esac
    done

    # We also add -Wa,--noexecstack to get ghc to generate .o files with
    # non-exectable stack. This it a hack until ghc does it itself properly.
    append-ghc-cflags assemble "-Wa,--noexecstack"
}

ghc_pkg_pretend() {
    if option !bootstrap && ! ghc --version &>/dev/null; then
        ewarn "You need GHC installed to compile GHC from source."
        ewarn "Try installing dev-lang/ghc[bootstrap]"
        die "Could not find a ghc to bootstrap with"
    fi
}

ghc_src_prepare() {
    ghc_setup_cflags

    edo sed -e "/fp_prog_ar/s/\[ar\]/[${AR}]/" \
            -i aclocal.m4

    autotools_src_prepare

    # Modify the ghc driver script to use GHC_CFLAGS
    edo sed -e "s|\$\$TOPDIROPT|\$\$TOPDIROPT ${GHC_CFLAGS}|" \
            -i "${WORK}/driver/ghc/Makefile"

    if option bootstrap ; then
        # The binary tarball's wrappers hard-code paths bad for bootstrapping
        edo sed -i -e "s:\(/usr/\):${WORKBASE}\1:g" \
            "${WORKBASE}"/usr/bin/ghc-${PV} \
            "${WORKBASE}"/usr/bin/ghc-pkg-${PV} \
            "${WORKBASE}"/usr/bin/ghci-${PV} \
            "${WORKBASE}"/usr/bin/hsc2hs \
            "${WORKBASE}"/usr/bin/runghc \
            "${WORKBASE}"/usr/lib*/${PNV}/package.conf.d/*
        case $(exhost --build) in
            x86_64-pc-linux-musl|x86_64-pc-linux-gnu|i686-pc-linux-gnu)
                :
            ;;
            *)
                die "Host not supported"
            ;;
        esac
        edo "${WORKBASE}"/usr/bin/ghc-pkg recache
    fi
}

ghc_src_configure() {
    # Initialize build.mk
    echo '# Exherbo changes' > mk/build.mk

    cat <<EOF >> mk/build.mk
# Put docs into the right place
docdir = /usr/share/doc/${PNVR}
htmldir = /usr/share/doc/${PNVR}/html

# Use GHC_CFLAGS flags when building ghc itself
SRC_HC_OPTS += ${GHC_CFLAGS}
SRC_CC_OPTS += ${CFLAGS} -Wa,--noexecstack
EOF

    if option doc; then
        echo "BUILD_SPHINX_HTML=YES" >> mk/build.mk
        echo "BUILD_SPHINX_PDF=NO" >> mk/build.mk
        echo "HADDOCK_DOCS=YES" >> mk/build.mk
    else
        echo "BUILD_SPHINX_HTML=NO" >> mk/build.mk
        echo "BUILD_SPHINX_PDF=NO" >> mk/build.mk
        echo "HADDOCK_DOCS=NO" >> mk/build.mk
    fi

    if option bootstrap ; then
        GHC_SRC_CONFIGURE_OPTIONS+=( --with-ghc=${WORKBASE}/usr/bin/ghc )
        export PATH="${WORKBASE}/usr/bin:${PATH}"
    fi

    if option llvm; then
        echo "GhcWithLlvmCodeGen=YES" >> mk/build.mk
    else
        echo "GhcWithLlvmCodeGen=NO" >> mk/build.mk
    fi

    GHC_SRC_CONFIGURE_OPTIONS+=(
        --with-system-libffi
        --with-ffi-includes=$(${PKG_CONFIG} --variable includedir libffi)
    )

    # host and build cause compilation (at least for 7.6.1) to fail, as they're intended for
    # cross-compilation.
    econf \
        AR=${AR} \
        --with-gcc=${CC} \
        --with-ld=${LD} \
        --with-nm=${NM} \
        --with-ar=${AR} \
        --with-ranlib=${RANLIB} \
        --target=$(exhost --target) \
        "${GHC_SRC_CONFIGURE_OPTIONS[@]}"

    # don't strip anything. Very useful when stage2 SIGSEGVs on you
    echo "STRIP_CMD = :" >> mk/build.mk
}

ghc_bundled_util_version() {
    local ver=$(sed -ne 's/^version:[ \t]*//ip' "${WORK}"/utils/${1}/${1}.cabal)
    [[ -n ${ver} ]] || die "could not determine version of bundled ${1}"
    echo ${ver}
}

ghc_src_install() {
    default_src_install

    if option doc; then
        # builtin xhtml is broken
        edo rm -rf "${IMAGE}"/usr/share/doc/${PNVR}/html/libraries/xhtml-*
        edo rm -rf "${IMAGE}"/usr/$(exhost --target)/lib/${PNV}/{,package.conf.d/}xhtml*
        # haddock broken because of xhtml
        edo rm -f "${IMAGE}"/usr/$(exhost --target)/lib/${PNV}/bin/haddock
        edo rm -f "${IMAGE}"/usr/$(exhost --target)/bin/haddock{,-*}
    else
        edo rm -rf "${IMAGE}"/usr/share/doc/${PNVR}/html
    fi

    # libraries/ is a partial dupe of the html/ dir hierarchy, but
    # containing only LICENSE files
    edo rm -rf "${IMAGE}"/usr/share/doc/${PNVR}/{LICENSE,libraries}

    dobashcompletion "${FILES}/ghc-bash-completion"

    # Alternatives:
    cd "${IMAGE}" || die "Failed to enter \$IMAGE=${IMAGE}"

    # Delete symlinks that we'll replace with alternatives
    local symlink
    for symlink in ghc ghci ghc-pkg runhaskell ; do
        [[ -f usr/$(exhost --target)/bin/${symlink} && -L usr/$(exhost --target)/bin/${symlink} ]] ||
            die "Expected ${IMAGE}/usr/$(exhost --target)/bin/${symlink} to be a symlink, but it isn't"
        edo rm usr/$(exhost --target)/bin/${symlink}
    done

    if option !bootstrap; then
        [[ -f usr/$(exhost --target)/bin/runghc && -L /usr/$(exhost --target)/bin/runghc ]] ||
            die "Expected ${IMAGE}/usr/$(exhost --target)/bin/runghc to be a symlink, but it isn't"
    fi
    edo rm usr/$(exhost --target)/bin/runghc

    # Version all executables, manual pages
    local executable manpage
    for executable in usr/$(exhost --target)/bin/* ; do
        if [[ ${executable} != *-${SLOT} ]]; then
            edo mv ${executable}{,-${SLOT}}
        fi
    done
    if [[ -d usr/share/man ]]; then
        for manpage in usr/share/man/man*/* ; do
            if [[ ${manpage} != *-${SLOT} ]]; then
                # find correct suffix from folder
                local suffix=${manpage#usr/share/man/man}
                suffix=${suffix%%/*}
                edo mv ${manpage} ${manpage%.${suffix}}-${SLOT}.${suffix}
            fi
        done
    fi

    # Record alternatives
    local alternatives=() src target

    for src in usr/$(exhost --target)/bin/*-${SLOT} usr/share/man/man*/*-${SLOT}.* ; do
        if [[ ${src} == usr/$(exhost --target)/bin/hsc2hs-${SLOT} ]]; then
            edo mv "${IMAGE}/${src}" "${IMAGE}/usr/$(exhost --target)/bin/hsc2hs-ghc-${SLOT}"
            local hsc2hs_pv=$(ghc_bundled_util_version hsc2hs)
            alternatives_for hsc2hs ${hsc2hs_pv}_ghc-${SLOT} ${hsc2hs_pv} /usr/$(exhost --target)/bin/hsc2hs /usr/$(exhost --target)/bin/hsc2hs-ghc-${SLOT}
        elif [[ -e "${src}" ]]; then
            target=${src/-${SLOT}}
            src=${src##*/}
            alternatives+=( "/${target}" "${src}" )
        fi
    done

    alternatives_for ${PN} ${SLOT} ${SLOT} \
        "${alternatives[@]}" \
        /usr/$(exhost --target)/bin/runhaskell /usr/$(exhost --target)/bin/runghc-${SLOT}

    edo sed -e "s,/usr/$(exhost --target)/bin/ghc,/usr/$(exhost --target)/bin/ghc-${SLOT}," \
        -i "${IMAGE}"/usr/$(exhost --target)/bin/runghc-${SLOT}

    # Used in ghc_pkg_prerm(), to unmerge ourselves cleanly
    if [[ -d ${IMAGE}/usr/$(exhost --target)/lib/${PNV}/package.conf.d ]] ; then
        insinto /usr/$(exhost --target)/lib/${PNV}/package.conf.d.shipped/
        doins -r "${IMAGE}"/usr/$(exhost --target)/lib/${PNV}/package.conf.d/*
    fi
    if [[ -f ${IMAGE}/usr/$(exhost --target)/lib/${PNV}/package.conf ]] ; then
        edo cp -p "${IMAGE}/usr/$(exhost --target)/lib/${PNV}/package.conf"{,.shipped}
    fi
}

ghc_src_test_expensive() {
    emake fulltest
}

ghc_pkg_prerm() {
    alternatives_pkg_prerm

    # Overwrite the (potentially) modified package.conf with a copy of the
    # original one, so that it will be removed during uninstall.
    # Code below can go once we drop ghc[<6.12.1]

    PKG="${ROOT}/usr/$(exhost --target)/lib/${PNV}/package.conf"

    if [[ -f ${PKG}.shipped ]] ; then
        cp -p "${PKG}"{.shipped,}
    fi

    [[ -f ${PKG}.old ]] && rm "${PKG}.old"
}

ghc_pkg_postinst() {
    alternatives_pkg_postinst
    # FIXME: ${ROOT}
    local script
    for script in /usr/$(exhost --target)/lib/${PNV}/registration/*/register.sh ; do
        [[ -f ${script} ]] || continue
        # let's not kill the installation just for this...
        echo "sh ${script}"
        sh "${script}" || ewarn "Reregistering ${script} failed!"
    done
}

